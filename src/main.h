#ifndef __MAIN_H_
#define __MAIN_H_

/* Provides uintN_t, uint_fastN_t, etc. (for N {8,16,32}) */
#include <stdint.h>

#define _BV(n) (1 << (n))

#endif // __MAIN_H_