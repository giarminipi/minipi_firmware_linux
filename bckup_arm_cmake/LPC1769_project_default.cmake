add_definitions(
  -D__NEWLIB__
  -D__CODE_RED
  -D__USE_CMSIS=CMSISv2p00_LPC17xx

  -Wall
  -Wshadow
  -Wcast-qual
  -Wwrite-strings
  -Winline

  -fmessage-length=80
  -ffunction-sections
  -fdata-sections

  -std=gnu99

  -mcpu=cortex-m3
  -mthumb
)

# if(DEFINED CMAKE_RELEASE)
#   add_definitions(-O2 -Os)
# else()
#   add_definitions(-O0 -g3 -DDEBUG)
# endif()



# if(DEFINED SEMIHOSTING_ENABLED)
#   set(LINKER_SCRIPT "${CMAKE_CURRENT_SOURCE_DIR}/LPC1769_semihosting.ld")
# endif()

# May break if split for 80 character line
set(LINKER_SCRIPT "${CMAKE_CURRENT_SOURCE_DIR}/LPC1769.ld")
set(CMAKE_EXE_LINKER_FLAGS "-nostdlib -Xlinker --gc-sections -mcpu=cortex-m3 -mthumb -T ${LINKER_SCRIPT}")

# Output
add_executable(${CMAKE_PROJECT_NAME}.axf ${SOURCES} LPC17xx/Device/Source/system_LPC17xx.c)
set_target_properties(${OUTPUT_NAME} PROPERTIES LINKER_LANGUAGE C)
# target_link_libraries(${OUTPUT_NAME} CMSISv2p00_LPC17xx UMD_LPC1769)

# Separate debug symbols into their own file
# set(FULL_OUTPUT_NAME ${EXECUTABLE_OUTPUT_PATH}/${CMAKE_PROJECT_NAME}.axf)
# add_custom_command(
#   TARGET ${OUTPUT_NAME}
#   POST_BUILD
#   COMMAND ${LPCXPRESSO_OBJCOPY} --only-keep-debug ${FULL_OUTPUT_NAME}
#                                 ${FULL_OUTPUT_NAME}.debug
#   COMMAND ${LPCXPRESSO_STRIP} -g ${FULL_OUTPUT_NAME}
# )