# Proyectos de referencia que usan CMake

This CMake was copy initially from https://github.com/vpetrigo/arm-cmake-toolchains/blob/master/examples/efm32/led/CMakeLists.txt
https://github.com/gpittarelli/umd-lpc1769

# Steps

install:

```bash
sudo apt install gcc-arm-none-eabi 
```
# References

[armgcc]: https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads
[clang]: http://releases.llvm.org/
[lpc1769]: https://www.keil.com/dd2/nxp/lpc1769/#/eula-container
